FROM registry.gitlab.com/jinhu/ruby:master
MAINTAINER Jin <jin@hukelou.com>

COPY Gemfile /app/
RUN  echo 'gem: --no-document' > ~/.gemrc \
 &&  apk add --no-cache openssl ca-certificates \
 &&  apk --update add --virtual build-dependencies build-base \
        postgresql-dev libc-dev linux-headers \
 &&  gem install bundler --no-ri --no-rdoc \
 &&  cd /app/ ; bundle install \
 &&  apk del build-dependencies
